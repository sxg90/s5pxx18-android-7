#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

ifeq ($(BOARD_CARLIFE_SUPPORT),true)

# carlife with android phone
PRODUCT_PACKAGES += \
	libbdcl \
	libdiagnose_usb_bdcl \
	bdcl

# carlife with iphone
ifeq ($(TARGET_ARCH),arm)
PRODUCT_PACKAGES += \
	libusb1.0 \
	libcnary \
	libplist \
	libplist++ \
	plist_cmp \
	plist_test \
	plist_util \
	libusbmuxdcommon \
	libusbmuxd \
	iproxy \
	libcrypto_openssl \
	libimobilecommon \
	libmobiledevice \
	ideviceinfo \
	idevicename \
	idevicepair \
	idevicesyslog \
	idevice_id \
	idevicebackup \
	idevicebackup2 \
	ideviceimagemounter \
	idevicescreenshot \
	ideviceenterrecovery \
	idevicedate \
	ideviceprovision \
	idevicedebugserverproxy \
	idevicediagnostics \
	idevicedebug \
	idevicenotificationproxy \
	idevicecrashreport \
	usbmuxd \
	libzip \
	ideviceinstaller

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/init.carlife.rc:root/init.carlife.rc \
	$(LOCAL_PATH)/iproxy.sh:system/bin/iproxy.sh
endif

endif ### BOARD_CARLIFE_SUPPORT ###
